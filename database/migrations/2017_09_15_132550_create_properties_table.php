<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_good')->unsigned();
            $table->string('brand');
            $table->string('model');
            $table->integer('width');
            $table->integer('height');
            $table->string('construction');
            $table->integer('diametr');
            $table->integer('index_load');
            $table->string('index_speed');
            $table->string('abbrev')->nullable();
            $table->string('runflat')->nullable();
            $table->string('camernost')->nullable();
            $table->string('suzon');
            $table->timestamps();

            $table->foreign('id_good')->references('id')->on('goods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
