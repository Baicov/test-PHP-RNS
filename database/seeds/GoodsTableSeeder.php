<?php

use Illuminate\Database\Seeder;

class GoodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Goods::create(['name' => 'Toyo H08 195/75R 16C 107/105S TL Летние']);
        \App\Goods::create(['name' => 'Pirelli Winter SnowControl serie 3 175/70 R14 84T TL Зимние (нешипованные)']);
        \App\Goods::create(['name' => 'BFGoodrich Mud-Terrain T/A KM2 235/85 R16 120/116Q TL Внедорожные']);
        \App\Goods::create(['name' => 'Pirelli Scorpion Ice & Snow 265/45 R21 104H TL Зимние (нешипованные)']);
        \App\Goods::create(['name' => 'Pirelli Winter SottoZero Serie II 245/45 R19 102V XL Run Flat * TL Зимние (нешипованные)']);
        \App\Goods::create(['name' => 'Nokian Hakkapeliitta R2 SUV/Е 245/70 R16 111R XL TL Зимние (нешипованные)']);
        \App\Goods::create(['name' => 'Pirelli Winter Carving Edge 225/50 R17 98T XL TL Зимние (шипованные)']);
        \App\Goods::create(['name' => 'Continental ContiCrossContact LX Sport 255/55 R18 105H FR MO TL Всесезонные']);
        \App\Goods::create(['name' => 'BFGoodrich g-Force Stud 205/60 R16 96Q XL TL Зимние (шипованные)']);
        \App\Goods::create(['name' => 'BFGoodrich Winter Slalom KSI 225/60 R17 99S TL Зимние (нешипованные)']);
        \App\Goods::create(['name' => 'Continental ContiSportContact 5 245/45 R18 96W SSR FR TL Летние']);
        \App\Goods::create(['name' => 'Continental ContiWinterContact TS 830 P 205/60 R16 92H SSR * TL Зимние (нешипованные)']);
        \App\Goods::create(['name' => 'Continental ContiWinterContact TS 830 P 225/45 R18 95V XL SSR FR * TL Зимние (нешипованные)']);
        \App\Goods::create(['name' => 'Hankook Winter I*Cept Evo2 W320 255/35 R19 96V XL TL/TT Зимние (нешипованные)']);
        \App\Goods::create(['name' => 'Mitas Sport Force+ 120/65 R17 56W TL Летние']);
    }
}
