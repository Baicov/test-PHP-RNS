<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>Document</title>
</head>
<body>
<div class="container">
    <table class="table table-striped table-bordered">
        <tr>
            <th class="success">
                <h1 align="center">Название товара<br></h1>
                <h5 align="center">(исходные данные)</h5>
            </th>
        </tr>
        @foreach($allGoods as $good)
            <tr class="@if($good->valid == 1)danger @else info @endif">
                <td>{{ $good->name }}</td>
            </tr>
        @endforeach
    </table>
    <form action="/" method="POST">
        <input class="btn btn-info" type="submit" value="Пропарсить исходные данные">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        {{--<a class="btn btn-info" src="{{ url('parseGoods') }}">Пропарсить исходные данные</a>--}}
    </form>
</div>
</body>
<script src="{{ asset('js/app.js') }}"></script>
</html>