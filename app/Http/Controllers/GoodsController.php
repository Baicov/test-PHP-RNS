<?php

namespace App\Http\Controllers;

use App\Goods;
use App\Properties;
use Illuminate\Http\Request;
use function PHPSTORM_META\type;

class GoodsController extends Controller
{

    /**
     * Отдаем на шаблон все товары (исходные данные, для наглядности)
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getGoodsForView()
    {
        $allGoods = $this->getAllGoods();
        return view('mainPage', compact('allGoods'));
    }

    /**
     * Получение всех товаров из таблицы Goods
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function getAllGoods()
    {
        return Goods::all();
    }

    /**
     * Выставляем статус 1 в таблицеProperties, означающий не допустимый товар
     * @param $id
     */
    private function updateForErrorValid($id)
    {
        Goods::where('id', '=', $id)->update(['valid' => 1]);
    }

    /**
     * Парсинг характеристик заданных по-умолчанию в жестком формате п.3-8
     * @param $validProp
     */
    private function parseDefaultProp($validProp) {
        foreach ($validProp as $key => $val) {
            $pos = strpos($val, '/', 1);
            $validWidth[$key] = substr($val, 0, $pos);
            $val = substr($val, $pos + 1, strlen($val));

            $pos = strpos($val, ' ', 1);
            $validHeight[$key] = substr($val, 0, $pos);
            $val = substr($val, $pos + 1, strlen($val));

            $validConstr[$key] = substr($val, 0, 1);
            $val = substr($val, 1, strlen($val));

            $validDiametr[$key] = substr($val, 0, $pos);
            $val = substr($val, $pos + 1, strlen($val)) . ' ';

            $pos = strpos($val, ' ', 1);
            $validIndexLoad[$key] = substr($val, 0, $pos - 1);
            $val = substr($val, $pos - 1, strlen($val));

            $pos = strpos($val, ' ', 1);
            $val = substr($val, 0, $pos);
            $validIndexSpeed[$key] = $val;

        }
        return ['width' => $validWidth, 'height' => $validHeight, 'constr' => $validConstr, 'diametr' => $validDiametr, 'indexLoad' => $validIndexLoad, 'indexSpeed' => $validIndexSpeed];
    }

    /**
     * Парсим данные
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function parseGoods()
    {
        $allGoods = $this->getAllGoods();
        $countItem = 0;
        foreach ($allGoods as $good) {
            $validBrand = starts_with($good->name, CheckValidController::BRAND);
            if (!$validBrand) {
                $this->updateForErrorValid($good->id);
            } else {
                for ($i = 0, $j = 1; $i <= strlen($good->name); $i++) {
                    $posSpace = strpos($good->name, ' ', 1);
                    if (substr($good->name, $i, $j) == ' ') {
                        $brand[$good->id] = substr($good->name, 0, $posSpace);
                        $good->name = substr($good->name, $posSpace + $j, strlen($good->name));

                        $findErrorIndex = preg_match_all('|[0-9]{3}\/|', $good->name, $resArrReplceIndex, PREG_OFFSET_CAPTURE);
                        if ($findErrorIndex != 1) {
                            $this->updateForErrorValid($good->id);
                        } else {
                            foreach ($resArrReplceIndex as $key) {
                                $model[$good->id] = substr($good->name, 0, $key[$countItem][$j] - 1);
                                $good->name = substr($good->name, $key[$countItem][$j], strlen($good->name) );

                                $findDefaultProperties = preg_match_all('/|\d{3}\/\d{2} [a-zA-Z]{1}\d{2} (?:\d{2}|\d{3})[a-zA-Z]|/', $good->name, $resArrReplaceProp, PREG_OFFSET_CAPTURE);
                                foreach ($resArrReplaceProp as $keyprop) {
                                    if (!empty($keyprop[$j][$countItem])) {
                                        $resDefaultProp[$good->id] = $keyprop[$j][$countItem];
                                    }
                                }
//                                echo '<pre>';
//                                print_r($resDefaultProp);
                                foreach ($resArrReplaceProp as $key) {
                                    if (!empty($key[$j][$countItem])) {
                                        $defaultProp[$good->id] = $key[$j][$countItem];
                                        $good->name = substr($good->name, $key[2][$j] + 1, strlen($good->name));
                                        for ($k = 0; $k <= strlen($good->name); $k++) {
                                            $posSpace = strpos($good->name, ' ', 1);
                                            if (substr($good->name, $k , $j) == ' ') {
                                                $findCustomProp = substr($good->name, 0, $posSpace);

                                                $validSuzon = str_contains($findCustomProp, CheckValidController::SUZON);
                                                $validRunflat = str_contains($findCustomProp, CheckValidController::RUNFLAT);
                                                $validCamernost = str_contains($findCustomProp, CheckValidController::CAMERNOST);

                                                if (!$validSuzon) {
                                                    if (!$validRunflat) {
                                                        if (!$validCamernost) {
                                                            $abbr[$good->id] = $findCustomProp;
                                                            $good->name = substr($good->name, $k + 1, strlen($good->name));
                                                            $posSpace = strpos($good->name, ' ', 1);
                                                            $findDefaultProp = substr($good->name, 0, $posSpace);
                                                            $validRunflat = str_contains($findDefaultProp, CheckValidController::RUNFLAT);
                                                            $validCamernost = str_contains($findDefaultProp, CheckValidController::CAMERNOST);
                                                            $validSuzon = str_contains($findDefaultProp, CheckValidController::SUZON);

                                                            if ($validRunflat) {
                                                                $runflat[$good->id] = $findDefaultProp;
                                                                $good->name = substr($good->name, $posSpace + 1, strlen($good->name));
                                                                $findDefaultProp = substr($good->name, 0, $posSpace);
                                                                $validCamernost = str_contains($findDefaultProp, CheckValidController::CAMERNOST);
                                                                if ($validCamernost) {
                                                                    $camernost[$good->id] = $findDefaultProp;
                                                                    $good->name = substr($good->name, $posSpace + 2, strlen($good->name));
                                                                } else {
                                                                    $camernost[$good->id] = null;
                                                                    $good->name = substr($good->name, $posSpace + 2, strlen($good->name));
                                                                }

                                                                $findDefaultProp = substr($good->name, 0, $posSpace);
                                                                $validSuzon = str_contains($findDefaultProp, CheckValidController::SUZON);
                                                                if ($validSuzon) {
                                                                    $suzon[$good->id] = $findDefaultProp;
                                                                    $validDefaultProp = array_intersect_key($resDefaultProp, $suzon);
                                                                    $resProp = $this->parseDefaultProp($validDefaultProp);
                                                                } else {
                                                                    $this->updateForErrorValid($good->id);
                                                                }
                                                            } else if ($validCamernost){

                                                                $runflat[$good->id] = null;
                                                                $camernost[$good->id] = $findDefaultProp;
                                                                $good->name = substr($good->name, $posSpace + 1, strlen($good->name));

                                                                $findDefaultProp = substr($good->name, 0, strlen($good->name));
                                                                $validSuzon = str_contains($findDefaultProp, CheckValidController::SUZON);
                                                                if ($validSuzon) {
                                                                    $suzon[$good->id] = $findDefaultProp;
                                                                    $validDefaultProp = array_intersect_key($resDefaultProp, $suzon);
                                                                    $resProp = $this->parseDefaultProp($validDefaultProp);
                                                                } else {
                                                                    $this->updateForErrorValid($good->id);
                                                                }

                                                            } else if ($validSuzon) {
                                                                $runflat[$good->id] = null;
                                                                $camernost[$good->id] = null;
                                                                $suzon[$good->id] = $findDefaultProp;
                                                                $validDefaultProp = array_intersect_key($resDefaultProp, $suzon);
                                                                $resProp = $this->parseDefaultProp($validDefaultProp);
                                                            } else {
                                                                $this->updateForErrorValid($good->id);
                                                            }
                                                        } else {
                                                            $abbr[$good->id] = null;
                                                            $runflat[$good->id] = null;

                                                            $posSpace = strpos($good->name, ' ', 1);
                                                            $findDefaultProp = substr($good->name, 0, $posSpace);
                                                            $camernost[$good->id] = $findDefaultProp;
                                                            $good->name = substr($good->name, $posSpace + 1, strlen($good->name));

                                                            $findDefaultProp = substr($good->name, 0, strlen($good->name));
                                                            $validSuzon = str_contains($findDefaultProp, CheckValidController::SUZON);

                                                            if ($validSuzon) {
                                                                $suzon[$good->id] = $findDefaultProp;
                                                                $validDefaultProp = array_intersect_key($resDefaultProp, $suzon);
                                                                $resProp = $this->parseDefaultProp($validDefaultProp);
                                                            } else {
                                                                $this->updateForErrorValid($good->id);
                                                            }
                                                        }

                                                    } else {
                                                        $abbr[$good->id] = null;
                                                        $posSpace = strpos($good->name, ' ', 1);
                                                        $findDefaultProp = substr($good->name, 0, $posSpace);
                                                        $runflat[$good->id] = $findDefaultProp;
                                                        $good->name = substr($good->name, $posSpace + 1, strlen($good->name));

                                                        $findDefaultProp = substr($good->name, 0, $posSpace);
                                                        $validCamernost = str_contains($findDefaultProp, CheckValidController::CAMERNOST);

                                                        if ($validCamernost) {
                                                            $camernost[$good->id] = $findDefaultProp;
                                                            $good->name = substr($good->name, $posSpace + 2, strlen($good->name));
                                                        } else {
                                                            $camernost[$good->id] = null;
                                                            $good->name = substr($good->name, $posSpace + 2, strlen($good->name));
                                                        }

                                                        $findDefaultProp = substr($good->name, 0, strlen($good->name));
                                                        $validSuzon = str_contains($findDefaultProp, CheckValidController::SUZON);

                                                        if ($validSuzon) {
                                                            $suzon[$good->id] = $findDefaultProp;
                                                            $validDefaultProp = array_intersect_key($resDefaultProp, $suzon);
                                                            $resProp = $this->parseDefaultProp($validDefaultProp);
                                                        } else {
                                                            $this->updateForErrorValid($good->id);
                                                        }
                                                    }
                                                } else {
                                                    $abbr[$good->id] = null;
                                                    $runflat[$good->id] = null;
                                                    $camernost[$good->id] = null;
                                                    $suzon[$good->id] = $findCustomProp;
                                                    $validDefaultProp = array_intersect_key($resDefaultProp, $suzon);
                                                    $resProp = $this->parseDefaultProp($validDefaultProp);
                                                }
                                                break;
                                            }
                                        }
                                    } else {
                                        $this->updateForErrorValid($good->id);
                                    }
                                }
                                break;
                            }
                        }
                        break;
                    }
                }
            }
        }
        $ids = array_keys($suzon);
        $resCount = 0;
        foreach ($ids as $id) {
            $checkId = Properties::where('id_good', $id)->count();
            if ($checkId) {
                Properties::where('id_good', '=', $id)->update(['brand' => $brand[$id], 'model' => $model[$id], 'width' => $resProp['width'][$id], 'height' => $resProp['height'][$id], 'construction' => $resProp['constr'][$id], 'diametr' => $resProp['diametr'][$id], 'index_load' => $resProp['indexLoad'][$id], 'index_speed' => $resProp['indexSpeed'][$id], 'abbrev' => $abbr[$id], 'runflat' => $runflat[$id], 'camernost' => $camernost[$id], 'suzon' => $suzon[$id], 'id_good' => $id]);
            } else {
                Properties::insert(['brand' => $brand[$id], 'model' => $model[$id], 'width' => $resProp['width'][$id], 'height' => $resProp['height'][$id], 'construction' => $resProp['constr'][$id], 'diametr' => $resProp['diametr'][$id], 'index_load' => $resProp['indexLoad'][$id], 'index_speed' => $resProp['indexSpeed'][$id], 'abbrev' => $abbr[$id], 'runflat' => $runflat[$id], 'camernost' => $camernost[$id], 'suzon' => $suzon[$id], 'id_good' => $id]);
            }
            $resCount++;
        }
        return redirect('/');
    }
}