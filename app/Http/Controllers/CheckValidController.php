<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CheckValidController extends Controller
{
    /**
     * Список допустимых брендов - Brand.
     */
    const BRAND = ['Nokian', 'BFGoodrich', 'Toyo', 'Continental', 'Hankook', 'Mitas', 'Pirelli'];

    /**
     * Список допустимых ранфлэтов - RunFlat.
     */
    const RUNFLAT = ['RunFlat', 'Run Flat', 'ROF', 'ZP', 'SSR', 'ZPS', 'HRS', 'RFT'];

    /**
     * Список допустимых вариантов камерности.
     */
    const CAMERNOST = ['TT', 'TL', 'TL/TT'];

    /**
     * Список допустимых сезонов для шин.
     */
    const SUZON = ['Зимние (шипованные)', 'Внедорожные', 'Летние', 'Зимние (нешипованные)', 'Всесезонные'];
}
